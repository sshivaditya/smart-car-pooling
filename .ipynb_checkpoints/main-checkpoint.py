from ContextModule.ContextProcessor import CheckContext
from ContextModule.ContextHandler import GenEntity
from ContextModule.DataSetGen import GenData

data,ImgArr = GenData()
cars,users = GenEntity(data,ImgArr)
CheckContext(cars,users)