import os
import sys
import numpy as np
from ContextModule.ContextErraticFailure import DistanceMeasure

sys.path.append("..")


class CarSystemProcessor:
    USERNAME = None
    MASKRESULT = None
    TEMPERATURE = None
    ADDRESS = None
    ACCELOROMETER = None
    MOTIONSENSOR = None
    TIMEOFOBS = None
    SOLENOIDLOCK = None
    ALARM = None
    MASKIMAGE = None
    def __init__(self,car,usr):
        self.MOTIONSENSOR = car.MotionSensor
        self.ACCELOROMETER = car.Accelrometer
        self.SOLENOIDLOCK = car.SolenoidLock
        self.ALARM = car.Alarm
        self.TIMEOFOBS = car.ToO
        self.USERNAME = usr.Name
        self.TEMPERATURE = usr.Temperature
        self.MASKRESULT = usr.MaskResult
        self.MASKIMAGE = usr.MaskImage
        self.ADDRESS = usr.Address
    def ContextCheck(self):
        print('\n----------------------------------User Details----------------------------------\n')
        print("\nUser's Name:",self.USERNAME)
        print("\n")
        print("\nUser's Adress:",self.ADDRESS)
        print('\n-----------------------------Context Aware Behaviour-----------------------------\n')
        #Behaviour 1: Detecting the presence of mask worn by the person who wishes to board the vehicle if present allow inside vehicle
        if((self.MASKRESULT == 'Mask Worn Correctly')):
            print('Behviour 1: Conditions Matching, Mask is Present and is worn correctly by the user. Opening the Vehicle Door')
            self.SOLENOIDLOCK = 0
        #Behaviour 2: Checking the temperature of the person and notifying the user about it if less than threshold allow the user into the vehicle
        if ((self.TEMPERATURE > 37.6) or (self.TEMPERATURE < 37)):
            print('Behaviour 2: Conditions Matching, Temperature within the threshold going to unlock the doors')
        #Behaviour 3: Check the accelrometer value after 150 seconds of locking of solenoid then lock the doors
        if ((self.ACCELOROMETER > 3) and (self.TIMEOFOBS <= 150)):
            print('Behaviour 3: Conditions Matching, Accelrometer value greater than zero for more than 150 Seconds. Going to Lock the doors')
            self.SOLENOIDLOCK = 1
        #Behaviour 4: Unlock the solenoid lock on reaching the destination when accelormeter value is zero for more than 120 seconds
        if ((self.ACCELOROMETER < 2) and (self.TIMEOFOBS <= 120)):
            print('Behaviour 4: Conditions Matching, Accelrometer value is close to zero more than 120 Seconds. Going to Un Lock the doors')
            self.SOLENOIDLOCK = 0
        #Behaviour 5: If motion is detected on the seats then lock the doors if not locked already after 145 seconds
        if((self.MOTIONSENSOR == 1)and(self.TIMEOFOBS >= 145)):
            print("Behviour 5: Conditions Matching,Motion is detected on the user's seat. Going to lock the doors ")
            self.SOLENOIDLOCK == 1
        #Behaviour 6: If user removes mask raise the alarm beeper
        if (self.ACCELOROMETER > 5):
            if (self.MASKRESULT != 'Mask Worn Correctly'):
                print('Behaviour 6: Matching conditions identified, User Tried to remove mask enroute Raise a alarm using the beeper')
                self.ALARM = 1
        print('\n')
        print('\n------------------------Failures and Erractic Behaviour------------------------\n')
        print('\n Hardware Failure \n')
        if((self.MASKRESULT == None)):
            print('Hardware Fault with Camera Detected. Going to use alternative source.\n')
            if ((self.TEMPERATURE > 37.6) or (self.TEMPERATURE < 37)):
                print('Conditions Matching, Temperature within the threshold going to unlock the doors.\n')
        if((self.MOTIONSENSOR == None)):
            print('Hardware Fault with Motion Sensor Detected. Going to use alternative source.\n')
            if ((self.ACCELOROMETER > 5)):
                print('Conditions Matching, Vehicle moving through the period of observation. Hence, the user is assume to be seated\n')
        print('\n Software Failure \n')
        if((self.ACCELOROMETER < 2) and (self.TIMEOFOBS <= 120)):       
            print('Using onboard Gyroscope we can calculate the distance \n')
            print('Destination Not reached !! Locking the Doors \n')
        if((self.MASKRESULT == None)&(self.MOTIONSENSOR == 1)):
            print('Motion Sensor Activated !!')
            print('Force Sensor indicates a absence of huaman seated !! Motion Sensor Wrongly activated !!! \n')
        print('\n Erratic Behaviour\n')
        if((self.MASKRESULT == None)):
           print('Camera Malfunctioning !! Eratic behaviour detected !!\n')
           print('Alternative Camera to be used \n')
        if((self.SOLENOIDLOCK == None)):
           print('Door malfunctioning !! \n')
           print('Activating unlock sequence using alternative pnuematic lock \n')
        print('\n------------------------Optimizing the Context Aware Rules------------------------\n')
        #Non deterministic activation fault
        if((self.MASKRESULT == None)):
            print("Non deterministic activation fault detected !! Because of Camera's erratic behaviour \n")
        if((self.SOLENOIDLOCK == None)):
            print("Non deterministic activation fault detected !! Because of Lock's erratic behaviour \n")   
        #Dead Context Fault
        if not ((self.MASKRESULT == 'Mask Worn Correctly') or (self.TEMPERATURE > 37.6) or (self.TEMPERATURE < 37) or ((self.ACCELOROMETER > 3) and (self.TIMEOFOBS <= 150))):
            print("Dead Context Fault detected !! \n")   
        #Dead Decision Fault
        if not ((self.MASKRESULT == 'Mask Worn Correctly') or (self.TEMPERATURE > 37.6) or (self.TEMPERATURE < 37) or ((self.ACCELOROMETER > 3) and (self.TIMEOFOBS <= 150))):
            print("Dead Decision Fault detected !! \n")   