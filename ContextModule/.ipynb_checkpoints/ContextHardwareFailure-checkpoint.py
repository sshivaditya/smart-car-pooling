'''
Introduce hardware failures into the dataset

Hardware Failures Identified : 

-> Camera Failure
-> PIR Sensor Failure

Correction :

-> Use Temperature Value to ascertain whether safe or not
-> Use accelorometer value to check whether seated or not
'''


from DataSetGen import *

def context_identify_image_hf(img):
  b,g,r = (img[100, 100])
  if (g == 255) and (r == 255) and (b == 10):
    return "Mask Worn Correctly"
  elif (g == 1) and (r == 1) and (b == 1) :
    return "Mask Worn Incorrectly"
  elif (g == 0) and (r == 0) and (b == 0) :
    return "Mask Not Worn"
  else:
    return None

def gen_50_hf():
  choice = [(255,255,10),(0,0,0),(1,1,1),(0,1,0)]
  width = 300
  height = 300
  r = []
  imgs = []
  for i in range(50):
    seq = random.choice(choice)
    img = create_image(width, height, rgb_color=seq)
    imgs.append(img)
    r.append(context_identify_image_hf(img))
  return imgs,r

def GenMotionSensorhf():
    Mot = []
    for i in range(50):
        d = random.randint(0,2)
        if d == 1:
            Mot.append(d)
        elif d == 0:
            Mot.append(d)
        else:
            Mot.append(None)       
    return Mot



def GenData():
    df = pd.DataFrame(columns = ['USERNAME','MASKRESULT','TEMPERATURE', 'ADDRESS','ACCELOROMETER','MOTIONSENSOR','TIMEOFOBS','SOLENOIDLOCK','ALARM'])
    ss,df['MASKRESULT'] = gen_50_hf()
    df['TEMPERATURE'] = GenTemperatureSensor()
    df['USERNAME'],df['ADDRESS'] = GenUserNameAndAddress() 
    df['ACCELOROMETER'] = GenAccelrometer() 
    df['MOTIONSENSOR'] = GenMotionSensorhf()
    df['TIMEOFOBS'] = GenPeriodOfObs()
    df['SOLENOIDLOCK'] = GenSolenoidLock()
    df['ALARM'] = GenAlarm()
    return df,ss