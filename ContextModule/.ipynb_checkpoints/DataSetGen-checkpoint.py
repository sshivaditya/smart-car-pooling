import pandas as pd
import random
import cv2
import numpy as np
import matplotlib.pyplot as plt
import re
import numpy as np
from faker import Faker

fake = Faker()

def create_image(width, height, rgb_color=(0, 0, 0)):
    image = np.zeros((height, width, 3), np.uint8)
    color = tuple(reversed(rgb_color))
    image[:] = color
    return image

def GenUserNameAndAddress():
    Name = []
    Add = []
    for i in range(50):
        Name.append(fake.name())
        Add.append(fake.address())
    return Name,Add

def context_identify_image(img):
  b,g,r = (img[100, 100])
  if (g == 255) and (r == 255) and (b == 10):
    return "Mask Worn Correctly"
  elif (g == 1) and (r == 1) and (b == 1) :
    return "Mask Worn Incorrectly"
  elif (g == 0) and (r == 0) and (b == 0) :
    return "Mask Not Worn"

def gen_50():
  choice = [(255,255,10),(0,0,0),(1,1,1)]
  width = 300
  height = 300
  r = []
  imgs = []
  for i in range(50):
    seq = random.choice(choice)
    img = create_image(width, height, rgb_color=seq)
    imgs.append(img)
    r.append(context_identify_image(img))
  return imgs,r

def GenMotionSensor():
    Mot = []
    for i in range(50):
        d = random.randint(0,1)
        Mot.append(d)
    return Mot

def GenSolenoidLock():
    Mot = []
    for i in range(50):
        d = random.randint(0,1)
        Mot.append(d)
    return Mot

def GenAlarm():
    Mot = []
    for i in range(50):
        d = random.randint(0,1)
        Mot.append(d)
    return Mot

def GenAccelrometer():
    Acc = []
    for i in range(50):
        d = random.randint(0,50)
        Acc.append(d)
    return Acc

def GenTemperatureSensor():
    Val = []
    for i in range(50):
        d = random.randint(36,39)
        Val.append(d)
    return Val
def GenPeriodOfObs():
  time = []
  for i in range(50):
    d = random.randint(110,152)
    time.append(d)
  return time
#Dataset
#USERNAME
#MASKIMAGE
#MASKRESULT
#TEMPERATURE
#LOCKSTATUS
#ADDRESS
#ACCELOROMETER
def GenData():
    df = pd.DataFrame(columns = ['USERNAME','MASKRESULT','TEMPERATURE', 'ADDRESS','ACCELOROMETER','MOTIONSENSOR','TIMEOFOBS','SOLENOIDLOCK','ALARM'])
    ss,df['MASKRESULT'] = gen_50()
    df['TEMPERATURE'] = GenTemperatureSensor()
    df['USERNAME'],df['ADDRESS'] = GenUserNameAndAddress() 
    df['ACCELOROMETER'] = GenAccelrometer() 
    df['MOTIONSENSOR'] = GenMotionSensor()
    df['TIMEOFOBS'] = GenPeriodOfObs()
    df['SOLENOIDLOCK'] = GenSolenoidLock()
    df['ALARM'] = GenAlarm()
    return df,ss

def GenDataFuncs(funcs):
    df = pd.DataFrame(columns = ['USERNAME','MASKRESULT','TEMPERATURE', 'ADDRESS','ACCELOROMETER','MOTIONSENSOR','TIMEOFOBS','SOLENOIDLOCK','ALARM'])
    ss,df['MASKRESULT'] = funcs[0]()
    df['TEMPERATURE'] = funcs[1]()
    df['USERNAME'],df['ADDRESS'] = funcs[2]() 
    df['ACCELOROMETER'] = funcs[3]() 
    df['MOTIONSENSOR'] = funcs[4]()
    df['TIMEOFOBS'] = funcs[5]()
    df['SOLENOIDLOCK'] = funcs[6]()
    df['ALARM'] = funcs[7]()
    return df,ss