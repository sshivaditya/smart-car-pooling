'''
Context Aware Reasonining

--Case Based

->When a PIR sensor malfuctions we can't infer whther the person is sitting or not.
So, using previous values, if non-zero for more than 30 seconds we assum thh person to be sitting.

->When the accelerometer stops working or malfunctions and based on the previous value of accelerartion
furing the previous time frame is then used for controlling the locks

--Device Based

-->When the temperature sensor fails, then data from the camera can be used for prediction

-->When the solenoid lock atttached to the door malfunctions and alternative pneumatic lock will be activated to unlock the door
'''


from DataSetGen import *


def GenMotionSensorR():
    Mot = []
    for i in range(50):
        d = random.randint(0,2)
        if d == 1:
            Mot.append(d)
        elif d == 0:
            Mot.append(d)
        else:
            Mot.append(None)       
    return Mot


def GenAccelrometerR():
    Acc = []
    for i in range(50):
        d = random.randint(-5,50)
        if (d<0):
            Acc.append(None)
        else:
            Acc.append(d)
    return Acc

def GenTemperatureSensorR():
    Val = []
    for i in range(50):
        d = random.randint(35,40)
        if (d>39)or(d<36):
            Val.append(None)
        else:
            Val.append(d)
    return Val

def GenSolenoidLockR():
    Mot = []
    for i in range(50):
        d = random.randint(0,2)
        if d == 1:
            Mot.append(d)
        elif d == 0:
            Mot.append(d)
        else:
            Mot.append(None)       
    return Mot

def GenData():
    df = pd.DataFrame(columns = ['USERNAME','MASKRESULT','TEMPERATURE', 'ADDRESS','ACCELOROMETER','MOTIONSENSOR','TIMEOFOBS','SOLENOIDLOCK','ALARM'])
    ss,df['MASKRESULT'] = gen_50()
    df['TEMPERATURE'] = GenTemperatureSensorR()
    df['USERNAME'],df['ADDRESS'] = GenUserNameAndAddress() 
    df['ACCELOROMETER'] = GenAccelrometerR() 
    df['MOTIONSENSOR'] = GenMotionSensorR()
    df['TIMEOFOBS'] = GenPeriodOfObs()
    df['SOLENOIDLOCK'] = GenSolenoidLockR()
    df['ALARM'] = GenAlarm()
    return df,ss