'''
Introduce Software failures into the dataset

Software Failures Identified : 

-> Door opened at Signal
-> PIR Getting Wrongly activated

Correction :

-> Check distance
-> Force Sensor
'''


from DataSetGen import *


def GenMotionSensorSF():
    Mot = []
    for i in range(50):
        d = random.randint(0,2)
        if d == 1:
            Mot.append(d)
        elif d == 0:
            Mot.append(d)
        else:
            Mot.append(None)       
    return Mot

def GenSolenoidLockSF():
    Mot = []
    for i in range(50):
        d = random.randint(0,2)
        if d == 1:
            Mot.append(d)
        elif d == 0:
            Mot.append(d)
        else:
            Mot.append(None)       
    return Mot


def GenData():
    df = pd.DataFrame(columns = ['USERNAME','MASKRESULT','TEMPERATURE', 'ADDRESS','ACCELOROMETER','MOTIONSENSOR','TIMEOFOBS','SOLENOIDLOCK','ALARM'])
    ss,df['MASKRESULT'] = gen_50()
    df['TEMPERATURE'] = GenTemperatureSensor()
    df['USERNAME'],df['ADDRESS'] = GenUserNameAndAddress() 
    df['ACCELOROMETER'] = GenAccelrometer() 
    df['MOTIONSENSOR'] = GenMotionSensorSF()
    df['TIMEOFOBS'] = GenPeriodOfObs()
    df['SOLENOIDLOCK'] = GenSolenoidLockSF()
    df['ALARM'] = GenAlarm()
    return df,ss