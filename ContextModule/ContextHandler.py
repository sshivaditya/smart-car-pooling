import os
import sys
import numpy as np

sys.path.append("..")


#Car
#->MotionSensor: Value
#->Accelrometer: Value
#->TemperatrueSensor: Value
#->SolenoidLock: Boolean
#User
#->Mask: Boolean
#->Address: Value

class Car:
    MotionSensor = None
    Accelrometer = None
    SolenoidLock = 'LOCKED'
    Alarm = None
    ToO = None
    def __init__(self,data):
        self.MotionSensor = data[0]
        self.Accelrometer = data[1]
        self.SolenoidLock = data[2]
        self.Alarm = data[3]
        self.ToO = data[4]
class User:
    Name = None
    Temperature  = None
    Address = None
    MaskImage = None
    MaskResult = None
    ToO = None
    def __init__(self,data): 
        self.Temperature = data[0]
        self.MaskImage = data[1]
        self.MaskResult = data[2]
        self.Name = data[3]
        self.Address = data[4]
        self.ToO = data[5]
    def check(self):
       print()


def GenEntity(df, ImgArr):
    cars = []
    users = []
    for i,row in df.iterrows():
        #'USERNAME','MASKRESULT','TEMPERATURE', 'ADDRESS','ACCELOROMETER','MOTIONSENSOR'
        username,maskreslt,temp,add,acc,motion,tos,sole,ala = row
        car = Car([motion,acc,sole,ala,tos])
        usr = User([temp,ImgArr[i],maskreslt,username,add,tos])
        cars.append(car)
        users.append(usr)
    return cars,users
        

