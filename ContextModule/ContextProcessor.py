import os
import sys
import numpy as np

sys.path.append("..")
from .ContextResolver import CarSystemProcessor

def CheckContext(cars,users):
    for c,u in zip(cars,users):
        CC = CarSystemProcessor(c,u).ContextCheck()